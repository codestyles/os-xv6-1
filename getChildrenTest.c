#include "types.h"
#include "user.h"


void 
printprint(int unique_number) {
    printf(1, "I am %d, PID: %d, getChildrenOfRunningParent: %d\n",unique_number ,getpid(), getchildren());
}


int 
main(void)
{
    int proc = fork();

    if(proc == 0) {
        //The first child!
        int proc2 = fork();

        if(proc2 == 0) {
            //The child of first child!
            printprint(4);
            
            exit();
        } else {
            
            printprint(3);
            exit();
        }

    } else {
        int proc2 = fork();

        if(proc2 == 0) {
            //The child of first child!
            printprint(2);
            
            exit();
        } else {
            // PARENT
            printprint(1);       
            exit();
        }
    }
}