#include "types.h"
#include "stat.h"
#include "user.h"

//printf(1, "\nchild: parent=%d child=%d getpid()=%d getppid()=%d getchildren=%d\n\n",parent, child, getpid(), getppid(), getchildren());

struct timevars {
    int createdTick;
    int terminatedTick;
    int sleptTick;
    int readyTick;
    int runningTick;
};

static int instances[26];

int main(void)
{
    changepolicy(2);

    for(int i = 0; i <= 25; i++) {
        instances[i] = 0;
    }
    instances[0] = fork();
    for(int i = 1; i <= 25; i++) {
        if(instances[i-1] != 0) {
            printf(1, "THIS IS %d \n", getpid());
            instances[i] = fork();
            if(instances[i] != 0) {
                printf(1, "%d CREATED.\n", instances[i]);
            } else {
                // Child
                changepriority(5 - ((i-1)/5));
            }
        }
    }

    if(instances[25] != 0) {
        // PARENT
       // printf(1, "HERE PAAARENT!! \n");
       // printf(1, "- %d - \n", getchildren(getpid()));
        struct timevars tv[25];
        for(int i = 1; i <= 25; i++) {
            waitforchild(&tv[i-1]);
        }
        sleep(500);
        printf(1, "\n\n############## RESULTS ##############\n\n");
        for(int i = 0; i < 25; i++) {
            printf(1, "\nPROC %d TAT: %d CBT: %d WT: %d\n", i, tv[i].terminatedTick - tv[i].createdTick, tv[i].runningTick, tv[i].terminatedTick - tv[i].createdTick - tv[i].runningTick);
        }
        long int tatAve = 0, cbtAve = 0, wtAve = 0;
        printf(1, "\n\n@@@@@@@@@@@@@@ AVERAGES @@@@@@@@@@@@@@\n\n");
        for (int k = 0; k < 5; k++) {
            for (int i = 0; i < 5; i++) {
                tatAve += tv[k*5 + i].terminatedTick - tv[k*5 + i].createdTick;
                cbtAve += tv[k*5 + i].runningTick;
                wtAve += tv[k*5 + i].terminatedTick - tv[k*5 + i].createdTick - tv[k*5 + i].runningTick;
            }
            tatAve /= 5;
            cbtAve /= 5;
            wtAve /= 5;
            printf(1, "\nPRIORITY %d AVERAGE TAT: %d CBT: %d WT: %d\n", 5-k, tatAve, cbtAve, wtAve);
        }

        tatAve = 0;
        cbtAve = 0;
        wtAve = 0;
        for(int i = 0; i < 25; i++) {
            tatAve += tv[i].terminatedTick - tv[i].createdTick;
            cbtAve += tv[i].runningTick;
            wtAve += tv[i].terminatedTick - tv[i].createdTick - tv[i].runningTick;
        }
        tatAve /= 25;
        cbtAve /= 25;
        wtAve /= 25;
        printf(1, "\n\n@@@@@@@@@@@@@@ TOTAL AVERAGE @@@@@@@@@@@@@@\n\n");
        printf(1, "\nAVERAGE TAT: %d CBT: %d WT: %d\n", tatAve, cbtAve, wtAve);

    }
    else {
        for (int i = 1; i <= 25; i++) {
            if (instances[i] == 0) {
                for (int j = 0; j < 1000; j++) {
                    printf(1, "[%d]: [%d]\n", i, j + 1);
                }
                break;
            }
        }
    }

    exit();

}

