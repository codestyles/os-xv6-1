// Long-term FIFO locks for processes
struct ticketlock {
  int locked;       // Is the lock held?
  struct spinlock lk; // spinlock protecting this sleep lock
  int pids[100];
  int tickets[100];
  int unlocked;
  int head;
  int tail;
  // For debugging:
  char *name;        // Name of lock.
  int pid;           // Process holding lock
};

