#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "ticketlock.h"

struct {
    struct spinlock lock;
    struct proc proc[NPROC];
} ptable;

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int
sys_getyear(void)
{
  return 1975;
}

int
sys_getchildren(void)
{
  int pid = myproc()->parent->pid;
  argint(0, &pid);
  struct proc *p;
  acquire(&ptable.lock);
  int output = 0;

  for(p = ptable.proc ; p < &ptable.proc[NPROC]; p++){
    if(p->parent->pid == pid){
      if(output == 0 )
        output = p->pid;
      else
        output = (output*100) + (p->pid);
      }   
  }
  release(&ptable.lock);
  return output;
}

int
sys_changepolicy(int policy)
{
    argint(0, &policy);
    if(policy < 0 || policy > 2)
        return -1;
    currentAlgo = policy;
    return 1;
}

int
sys_changepriority(int priority)
{
    argint(0, &priority);
    if(priority < 1 || priority > 5)
        return -1;
    myproc()->priority = priority;
    return 1;
}

int
sys_waitforchild(struct timevars* tv)
{
    argptr(0, (void*)&tv, sizeof(struct timevars));
    return waitforchildproc(tv);
}

int sys_ticketlockinit(void) {
    tlock = (void*) kalloc();
    initticketlock(tlock, "tlock");
    return 0;
}

int sys_ticketlocktest(void) {
    acquireticketlock(tlock);
    releaseticketlock(tlock);
    return tlock->locked;
}

int sys_rwinit(void) {
    rwlock = (void*) kalloc();
    rwmutex = (void*) kalloc();
    initticketlock(rwlock, "rwlock");
    initticketlock(rwmutex, "rwmutex");
    rcount = 0;
    sharedCounter = 0;
    return 0;
}

int sys_rwtest(int pattern){
    argint(0, &pattern);
    if(pattern) {
        acquireticketlock(rwlock);
        //
        //microdelay(500000);
        sharedCounter++;
        //
        releaseticketlock(rwlock);
    } else {
        int data;
        acquireticketlock(rwmutex);
        rcount++;
        if(rcount == 1)
            acquireticketlock(rwlock);
        releaseticketlock(rwmutex);
        //
        //microdelay(100000);
        data = sharedCounter;
        //
        acquireticketlock(rwmutex);
        rcount--;
        if(rcount == 0)
            releaseticketlock(rwlock);
        releaseticketlock(rwmutex);
        return data;
    }

    return 0;
}
