#include "types.h"
#include "stat.h"
#include "user.h"


struct timevars {
    int createdTick;
    int terminatedTick;
    int sleptTick;
    int readyTick;
    int runningTick;
};

static int instances[11];

int main(void)
{
    changepolicy(1);

    for(int i = 0; i <= 10; i++) {
        instances[i] = 0;
    }
    instances[0] = fork();
    for(int i = 1; i <= 10; i++) {
        if(instances[i-1] != 0) {
            printf(1, "THIS IS %d \n", getpid());
            instances[i] = fork();
            if(instances[i] != 0) {
                printf(1, "%d CREATED.\n", instances[i]);
            }
        }
    }

    if(instances[10] != 0) {
        // PARENT
       // printf(1, "HERE PAAARENT!! \n");
       // printf(1, "- %d - \n", getchildren(getpid()));
        struct timevars tv[10];
        for(int i = 1; i <= 10; i++) {
            waitforchild(&tv[i-1]);
        }
        sleep(500);
        printf(1, "\n\n############## RESULTS ##############\n\n");
        for(int i = 0; i < 10; i++) {
            printf(1, "\nPROC %d TAT: %d CBT: %d WT: %d\n", i, tv[i].terminatedTick - tv[i].createdTick, tv[i].runningTick, tv[i].terminatedTick - tv[i].createdTick - tv[i].runningTick);
        }
        long int tatAve = 0, cbtAve = 0, wtAve = 0;
        for(int i = 0; i < 10; i++) {
            tatAve += tv[i].terminatedTick - tv[i].createdTick;
            cbtAve += tv[i].runningTick;
            wtAve += tv[i].terminatedTick - tv[i].createdTick - tv[i].runningTick;
        }
        tatAve /= 10;
        cbtAve /= 10;
        wtAve /= 10;
        printf(1, "\n\n@@@@@@@@@@@@@@ AVERAGES @@@@@@@@@@@@@@\n\n");
        printf(1, "\nAVERAGE TAT: %d CBT: %d WT: %d\n", tatAve, cbtAve, wtAve);

    }
    else {
        for (int i = 1; i <= 10; i++) {
            if (instances[i] == 0) {
                for (int j = 0; j < 1000; j++) {
                    printf(1, "[%d]: [%d]\n", getpid(), j + 1);
                }
                break;
            }
        }
    }

    exit();

}

