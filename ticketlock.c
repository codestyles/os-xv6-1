// Sleeping FIFO locks

#include "types.h"
#include "defs.h"
#include "param.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"
#include "ticketlock.h"

/*struct ticketlock {
    uint locked;       // Is the lock held?
    struct spinlock lk; // spinlock protecting this sleep lock
    int pids[100];
    uint tickets[100];
    uint unlocked;

    // For debugging:
    char *name;        // Name of lock.
    int pid;           // Process holding lock
};*/

void
initticketlock(struct ticketlock *lk, char *name)
{
  initlock(&lk->lk, "ticket lock");
  lk->name = name;
  lk->locked = 0;
  lk->pid = 0;
  for(int i=0;i<100;i++){
      lk->pids[i] = -1;
      lk->tickets[i] = 0;
  }
  lk->unlocked = 0;
  lk->head = 0;
  lk->tail = 0;
}

void
acquireticketlock(struct ticketlock *lk)
{
  acquire(&lk->lk);
  lk->pids[lk->tail] = myproc()->pid;
  fetch_and_add(&lk->locked, 1);
  lk->tickets[lk->tail] = (lk->locked);
  fetch_and_add(&lk->tail, 1);
  if(lk->tail >= 100) {
      lk->tail = 0;
  }

  while ((lk->locked - lk->unlocked > 1)) {
      sleep(lk, &lk->lk);
  }
  lk->pid = myproc()->pid;
  release(&lk->lk);
}

void
releaseticketlock(struct ticketlock *lk)
{
  acquire(&lk->lk);
  fetch_and_add(&lk->unlocked, 1);
  int j = lk->unlocked;
  fetch_and_add(&j, 1);
  for(int i = lk->head; (lk->tail >= lk->head ? i < lk->tail : i < lk->tail + 100); i++){

      if(lk->tickets[i%100] == (j)) {
          wakeupbypid(lk->pids[i%100]);
          fetch_and_add(&i, 1);
          lk->head = i;
          if(lk->head >= 100) {
              lk->head = 0;
          }
          break;
      }
  }
  release(&lk->lk);
}

int
holdingticketlock(struct ticketlock *lk)
{
  int r = 0;
  acquire(&lk->lk);
    int j = lk->unlocked;
    fetch_and_add(&j, 1);
    for(int i = lk->head; (lk->tail >= lk->head ? i < lk->tail : i < lk->tail + 100); i++){
        if(lk->tickets[i%100] == (j)) {
            r = (lk->pids[i%100] == myproc()->pid);
            break;
        }
    }
  release(&lk->lk);
  return r;
}



